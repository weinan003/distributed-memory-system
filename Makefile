CC=gcc 
CFLAGS =-O2 -g -Wall -std=gnu99


all : libsm.a dsm

test : dsm app share matmul

dsm: dsm.o dsm_alloc_handler.o 
	$(CC) $(CFLAGS) -o dsm dsm.o dsm_alloc_handler.o 

app: app.c libsm.a sm.h sm_common.h
	$(CC) $(CFLAGS) -o app app.c sm.h libsm.a sm_common.h

share: share.c libsm.a sm.h sm_common.h
	$(CC) $(CFLAGS) -o share share.c sm.h libsm.a sm_common.h

matmul: matmul.c libsm.a sm.h sm_common.h
	$(CC) $(CFLAGS) -o matmul matmul.c sm.h libsm.a sm_common.h

dsm.o: dsm.c dsm dsm.h 
	$(CC) $(CFLAGS) -c dsm.c

dsm_alloc_handler.o: dsm_alloc_handler.c dsm.h
	$(CC) $(CFLAGS) -c dsm_alloc_handler.c

libsm.a : libsm.o
	ar crs libsm.a libsm.o

libsm.o: sm.c sm.h sm_common.h
	$(CC) $(CFLAGS) -o libsm.o -c sm.c

.PHONY : clean
clean:
	-rm -f *.o *.a m1test dsm app share matmul *.gch
