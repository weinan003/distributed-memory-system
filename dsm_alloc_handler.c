#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <poll.h>
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>

#include "dsm.h"

#define INFTIM -1
#define PAGENOOWNER -1

char buf[5000] = {0};
int *cli_waiting; 



    static void
alloc_send_all(sm_rpc_msg_t *msg)
{
    for(int i = 0;i < host_Env.nNodes ;i ++)
    {
        if(host_Env.sockpoll[i].fd > 0)
            sm_send(host_Env.sockpoll[i].fd, msg, NULL);
    }
}


    void 
handle_socket_close(int i)
{
    if(host_Env.sync_tag_arr[i] == STOP)
    {
        close(host_Env.sockpoll[i].fd);
        host_Env.sockpoll[i].fd = -1;

        int j = 0;
        for(; j < host_Env.nNodes ;j++)
        {
            if(host_Env.sockpoll[j].fd > 0 
                    || host_Env.sync_tag_arr[j] != STOP ) 
                break;
        }

        if(j == host_Env.nNodes)
        {
            DEBUG_LOG("ALL CLIENT EXITS NOW,SERVER EXIT\n");
            host_Env.finish = 1;
        }
    }
    else
    {
        ERR_LOG("client %d socket close without permission,server kill all\n",i);
        INIT_MSG(msg,SM_RPC_KILL,-1,0,0,0);
        alloc_send_all(&msg);
        clean_exit(2);
    }
}

    void
dsm_alloc_run()
{
    LOG_INIT;
    cli_waiting = (int *)malloc(sizeof(int) * host_Env.nNodes);
    for (int i = 0; i < host_Env.nNodes; i++)
    {
        cli_waiting[i] = -1;
    }

    while(!host_Env.finish)
    {
        poll(host_Env.sockpoll,host_Env.nNodes, INFTIM);

        for(int i = 0;i < host_Env.nNodes;i ++)
        {
            if(host_Env.sockpoll[i].fd < 0) continue;

            if(host_Env.sockpoll[i].revents & POLLRDNORM )
            {
                ssize_t len;
                sm_rpc_msg_t msg;
                len = recv(host_Env.sockpoll[i].fd, buf, MSGLEN, MSG_WAITALL);
                if(len > 0)
                {
                    while(len < MSGLEN) 
                    {
                        ssize_t tmplen = recv(host_Env.sockpoll[i].fd, buf+len, MSGLEN-len, MSG_WAITALL);
                        if (tmplen > 0) len += tmplen; 
                    }


                    sm_rpc_msg_deseri(&msg, buf);
                    alloc_msg_handler(i,&msg);
                }
                else if (len < 0) continue;
                else
                {
                    handle_socket_close(i);
                }
            }
        }
    }
}


    void
alloc_handle_rfault(int index,sm_rpc_msg_t *msg)
{
    int pageid =  msg->para1;
    ssize_t pagesize = getpagesize();

    if(pageid < 0 || pageid >= host_Env.next_alloc)
    {
        INIT_MSG(MSG,SM_SEG_RFAULT_GRANT,-1,0,-1,0);
        sm_send(host_Env.sockpoll[index].fd,&MSG,NULL);
        LOG_RF(msg->nid, pageid);
        return;
    }

    void* page_content = host_Env.shared_mem + pagesize * pageid;

    LOG_RF(index,pageid);
    /* if pagetable => 0 it represents sockfd hold the write authority */
    if(host_Env.pagetable[pageid] != PAGENOOWNER)
    {
        int hasreq = 0;
        for (int i = 0; i < host_Env.nNodes; i ++)
        {
            if (cli_waiting[i] == pageid) 
            {
                hasreq = 1;
                break;
            }	
        }

        if(hasreq == 0)
        {
            int owner = host_Env.pagetable[pageid];
            INIT_MSG(MSG,SM_SEG_REVOK_W,-1,0,pageid,0);
            sm_send(host_Env.sockpoll[owner].fd,&MSG,NULL);
        }

        cli_waiting[index] = pageid;
    }
    else
    {
        INIT_MSG(rmsg,SM_SEG_RFAULT_GRANT,-1,pagesize,pageid,0);
        sm_send(host_Env.sockpoll[index].fd,&rmsg,page_content);
    }

}


    void
alloc_handle_wfault(int index,sm_rpc_msg_t *msg)
{
    int pageid = msg->para1;
    if(host_Env.pagetable[pageid] != PAGENOOWNER && 
            host_Env.pagetable[pageid] != index)
    {
        /* Requesting write permission failed, let client restart from read 
         * permission */
        INIT_MSG(MSG,SM_SEG_WFAULT_GRANT,-1,0,-1,(void*)pageid);
        sm_send(host_Env.sockpoll[index].fd,&MSG,NULL);
        return;
    }


    LOG_WF(index,pageid);
    INIT_MSG(CMSG,SM_SEG_REVOK_R,-1,0,pageid,0);
    INIT_MSG(RMSG,SM_SEG_WFAULT_GRANT,-1,0,pageid,0);

    for(int i = 0;i < host_Env.nNodes ;i ++)
    {
        if( i == index )
            sm_send(host_Env.sockpoll[i].fd,&RMSG,NULL);
        else
            sm_send(host_Env.sockpoll[i].fd,&CMSG,NULL);
    }

    host_Env.pagetable[pageid] = index;
}

    void 
alloc_handle_bcast(int index,sm_rpc_msg_t* msg)
{
    static void* sync_addr;
    if(msg->para2 != 0)
        sync_addr = msg->para2;

    host_Env.nbarrier --;

    if(host_Env.nbarrier == 0)
    {
        INIT_MSG(reply_msg,SM_RPC_BCAST_REPLY,-1,0,0,sync_addr);
        alloc_send_all(&reply_msg);
        host_Env.nbarrier = host_Env.nNodes;
    }
}

    int 
alloc_handle_pageback(int index,sm_rpc_msg_t* msg)
{
    if(msg->payloadlen)
    {
        int pageid = msg->para1;
        ssize_t len = 0;
        if(pageid >= 0 && pageid < host_Env.next_alloc)
        {
            while(len < msg->payloadlen)
            {
                ssize_t tmplen = recv(host_Env.sockpoll[index].fd,
                        host_Env.shared_mem + pageid * getpagesize() + len,
                        msg->payloadlen - len,MSG_WAITALL);
                if (tmplen > 0) len += tmplen;
            }

            LOG_RELEASE(index,pageid);
            ssize_t pagesize = getpagesize();
            /* send back page to who request it */
            INIT_MSG(rmsg,SM_SEG_RFAULT_GRANT,-1,pagesize,pageid,0);
            for(int i = 0; i < host_Env.nNodes; i++) 
            {
                if (cli_waiting[i] == pageid)
                {
                    sm_send(host_Env.sockpoll[i].fd, &rmsg, 
                            host_Env.shared_mem+pageid*pagesize);
                    cli_waiting[i] = -1;
                }
            }
            host_Env.pagetable[pageid] = PAGENOOWNER;	
        }
    }

    return 0;
}


    int
alloc_handle_barrier(int index,sm_rpc_msg_t *msg)
{
    /* in millstone one , server set sync tag direct to SYNCDOWN */
    host_Env.nbarrier --;

    if(host_Env.nbarrier == 0)
    {
        INIT_MSG(msg, SM_RPC_RESUME, -1, 0, 0, 0);
        alloc_send_all(&msg);
        host_Env.nbarrier = host_Env.nNodes;
    }

    return 0;
}


    int 
alloc_handle_malloc(int index,sm_rpc_msg_t* msg)
{
    unsigned int alloc_num = msg->para1;

    if(alloc_num > 0 && alloc_num + host_Env.next_alloc < PAGENUMS)
    {
        unsigned int startpage = host_Env.next_alloc;
        unsigned int endpage = startpage + alloc_num ;

        host_Env.next_alloc += alloc_num;
        for(int i = startpage;i < endpage;i ++)
            host_Env.pagetable[i] = index;

        LOG_MALLOC(index, startpage, endpage - 1) ;
        INIT_MSG(reply_msg,SM_RPC_MALLOC_GRANT,-1,0,startpage,0);
        sm_send(host_Env.sockpoll[index].fd, &reply_msg, NULL);
    }
    else
    {
        ERR_LOG("socket slot = %d apply %u page,but allocator do not have enough pages\n"
                ,index,alloc_num);
        INIT_MSG(reply_msg,SM_RPC_MALLOC_GRANT,-1,0,-1,0);
        sm_send(host_Env.sockpoll[index].fd, &reply_msg, NULL);
    }
    return 0;
}


    int
alloc_handle_error(int id)
{
    ERR_LOG("Client %d hit ERROR, force exit\n", id);
    INIT_MSG(msg, SM_RPC_KILL, -1, 0,0,0);
    alloc_send_all(&msg);

    clean_exit(1);
    return 0;
}

    int
alloc_handle_exit(int id)
{
    if(host_Env.sync_tag_arr[id] != STOP)
    {
        host_Env.sync_tag_arr[id] = STOP;
        host_Env.nZNodes ++;
        if(host_Env.nZNodes == host_Env.nNodes)
        {
            DEBUG_PRINTF("RECEIVE ALL EXIT APPLICATION\n");
            INIT_MSG(msg, SM_RPC_EXIT_GRANT,-1,0,0,0);
            alloc_send_all(&msg);
            clean_exit(0);
        }
    }

    return 0;
}


    void
alloc_msg_handler(int index,sm_rpc_msg_t *msg)
{
    switch (msg->type) 
    {
        case SM_RPC_BARRIER:
            alloc_handle_barrier(index,msg);
            break;
        case SM_RPC_EXIT_APPLY:
            alloc_handle_exit(index);
            break;
        case SM_RPC_HEART:
            sm_send(host_Env.sockpoll[index].fd, msg, NULL);
            break;
        case SM_RPC_MALLOC:
            alloc_handle_malloc(index,msg);
            break;
        case SM_RPC_BCAST:
            alloc_handle_bcast(index,msg);
            break;
        case SM_SEG_RFAULT:
            alloc_handle_rfault(index,msg);
            break;
        case SM_SEG_WFAULT:
            alloc_handle_wfault(index,msg);
            break;
        case SM_RPC_PAGE_RETURN:
            alloc_handle_pageback(index,msg);
            break;
        case SM_RPC_ERROR:
            alloc_handle_error(index);
        default:
            ERR_LOG("Received Unknown message type %d from %d,socket slot = %d\n"
                    , msg->type,msg->nid,index);
    }
}


    void
init_signal_handle()
{
    signal(SIGCHLD,sig_chld);
    signal(SIGINT, sig_int);
}

void sig_chld(int signo)
{
    int stat;
    int pid;


    while(1)
    {
        pid = wait(&stat);
        if (pid == ECHILD) return;
        else if (pid == EINTR) continue;
        else break;
    }

    if (pid == -1) return; // returned from terminated system(), ignore

    if(stat)
    {
        clean_exit(1);
    }
}

void sig_int(int signo)
{
    DEBUG_PRINTF("Got signal %d, clean shutting down\n", signo);
    INIT_MSG(msg, SM_RPC_KILL, -1, 0,0,0);
    alloc_send_all(&msg);	
    clean_exit(1);
}
