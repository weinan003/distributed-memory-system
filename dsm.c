#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/mman.h>

#include "dsm.h"


#define HOSTNAME "localhost"
#define DEFAULT_FILE "hosts"


static void
print_help()
{
    printf(
            "Usage: dsm [OPTION]... EXECUTABLE-FILE NODE-OPTION...\n"
            "\n"
            "  -H HOSTFILE list of host names\n"
            "  -h          this usage message\n"
            "  -l LOGFILE  log each significant allocator action to LOGFILE \n"
            "              (e.g., read/write fault, invalidate request)\n"
            "  -n N        fork N node processes\n"
            "  -v          print version information\n"
            "\n"
            "Starts the allocator, which forks N copies (one copy if -n not given) of\n"
            "EXECUTABLE-FILE.  The NODE-OPTIONs are passed as arguments to the node\n"
            "processes.  The hosts on which node processes are started are given in\n"
            "HOSTFILE, which defaults to `hosts'.  If the file does not exist,\n"
            "`localhost' is used."
          );
}


static void
parse_args(int argc, char *argv[], int *nNodes,
        char **hostfile, char **logfile,
        char **exefile, int *nodeargc, char **nodeargv[])
{
    extern char *optarg;
    extern int optind, optopt;
    int errflg = 0, hostflg = 0, logflg = 0, nNodeflg = 0;

    int c;
    while ((c = getopt(argc, argv, ":vhl:n:H:")) != -1) {
        switch(c) {
            case 'H':
                if (hostflg++) {
                    errflg++;
                    break;
                }
                *hostfile = optarg;
                break;
            case 'l':
                if (logflg++) {
                    errflg++;
                    break;
                }
                *logfile = optarg;
                break;
            case 'n':
                if (nNodeflg++) {
                    errflg++;
                    break;
                }
                *nNodes = atoi(optarg);
                break;
            case 'v':
                printf("Version: assignemnt 2,milestone two\n");
                clean_exit(0);
            case ':':
                fprintf(stderr, "Option -%c requires an operand\n", optopt);
                clean_exit(2);
            case '?':
            case 'h':
            default:
                print_help();
                clean_exit(0);
        }
    }

    if (errflg) {
        print_help();
        clean_exit(1);
    }

    if (optind == argc) {
        fprintf(stderr, "No executable file\n");
        clean_exit(2);
    }
    /* store node process path and the parameters pass to it*/
    *exefile = argv[optind];
    *nodeargv = &argv[optind+1];
    *nodeargc = argc - optind - 1;

    /* if no exefile exits then exit */
    if(access(*exefile,R_OK) != F_OK)
    {
        ERR_LOG("open exefile = %s failed:",*exefile);
        perror("");
        clean_exit(2);
    }
}


/*
 * parse hostfile content and retrieve host address
 */
static void
get_remote_hosts(char *hostfile, int *nhost, char **hostlist[])
{
    /* if hostfile cannot find , assign nhost as 1 and ip for localhost */
    if(access(hostfile,R_OK) != F_OK)
    {
        *nhost = 1;
        *hostlist = malloc(*nhost * sizeof(char *));
        *hostlist[0] = HOSTNAME;
        return;
    }

    *nhost = 0;
    FILE *fp = NULL;
    int c = 0, lc = 0;

    /* Count number of lines in hostfile */
    fp = fopen(hostfile, "r");
    while((c=fgetc(fp)) != EOF)
    {
        if (c == '\n') (*nhost)++;
        lc = c;
    }
    if (lc != '\n') (*nhost)++;

    *hostlist = (char **)malloc((*nhost) * sizeof(char *));

    /* set file offset to 0 */
    rewind(fp);

    /* Put each record into hostlist */
    char *line = NULL;
    size_t len;
    ssize_t read;
    for (int nline = 0;
         nline < *nhost && (read = getline(&line, &len, fp)) != -1;
         nline++) {
        if (line[read-1] == '\n') line[read-1] = '\0';
        (*hostlist)[nline] = malloc((strlen(line) - 1) * sizeof(char));
        strcpy((*hostlist)[nline], line);
    }
    fclose(fp);
}


/*
 * Fork processes to start client processes
 * The host machines selected for client process based on round-robin policy
 */
static void
start_nodes(char *hostlist[], int nhost, int nNodes, char *exefile,
            int nodeargc, char *nodeargv[], char *alloc_IP, int alloc_port)
{
    int curHost = 0;

    for (int nid = 0; nid < nNodes; nid++, curHost = (curHost + 1) % nhost) {
        pid_t pid = fork();
        if(pid == 0)
        {
            char command[1024] = {0};

            sprintf(command, "ssh %s %s %s %d %d %d %lu ", hostlist[curHost],
                    exefile, alloc_IP, alloc_port, nNodes, nid,
                    (long unsigned int)host_Env.shared_mem);

            for (int j = 0; j < nodeargc; j++) {
                strcat(command, nodeargv[j]);
                strcat(command," ");
            }

            DEBUG_LOG("ssh command:\"%s\"\n", command);

            int exitcode = system(command);
            pid_t childpid = getpid();
            DEBUG_LOG("child proc %d, nid %d, exit with code %d\n",
                         childpid, nid, exitcode);
            exit(exitcode);
        }
        else {
            host_Env.childproc[nid] = pid;
        }
    }
}
static int 
init_sharedmemory()
{
    void *maxaddr = sbrk(0);
    DEBUG_LOG("ALLOC HEAP = %p\n",maxaddr);
    for(int slot_id = 0;slot_id < host_Env.nNodes;slot_id++)
    {
        struct sockaddr_in cin;
        int addrlen = sizeof(cin);
        host_Env.sockpoll[slot_id].fd = accept(host_Env.sockfd
                , (struct sockaddr *) &(cin)
                ,(socklen_t *)&(addrlen));
        host_Env.sockpoll[slot_id].events = POLLRDNORM;

        sm_rpc_msg_t msg;
		while(sm_recv(host_Env.sockpoll[slot_id].fd, &msg, NULL)!=MSGLEN);
        void * cli_brk = msg.para2;
        if(cli_brk > maxaddr)
            maxaddr = cli_brk;

        DEBUG_LOG("Cli %d HEAP = %p\n",slot_id,cli_brk);
    }

    int pagesize = getpagesize();
    maxaddr += pagesize;
    maxaddr = (void *)((unsigned long)maxaddr & (~(0xFFF)));

    host_Env.shared_mem = mmap(maxaddr,pagesize * PAGENUMS,PROT_READ | PROT_WRITE,
            MAP_FIXED |MAP_PRIVATE | MAP_ANONYMOUS,-1,0);

	DEBUG_LOG("apply start %p allocated shared mem start %p\n",maxaddr, host_Env.shared_mem);
    if(host_Env.shared_mem == MAP_FAILED)
    {
        perror("mmap failed");
        exit(1);
    }

    host_Env.pagetable = (int *)malloc(sizeof(int) * PAGENUMS);
    bzero(host_Env.pagetable,PAGENUMS);

    host_Env.next_alloc = 0;

    for(int i = 0;i < host_Env.nNodes;i++)
    {
        INIT_MSG(rmsg,SM_RPC_INIT_REPLY,-1,0,i,maxaddr);
        sm_send(host_Env.sockpoll[i].fd,&rmsg,NULL);
    }

	return 0;
}

static int
init_socket()
{
    if((host_Env.sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
        return -1;

    char hostname[100] = {0};
    gethostname(hostname, 99);

    host_Env.my_hostname = malloc(strlen(hostname) * sizeof(char) + 1);
    bzero(host_Env.my_hostname,strlen(hostname) * sizeof(char) + 1);

    strcpy(host_Env.my_hostname , hostname);

    struct hostent *host = gethostbyname(host_Env.my_hostname);

    struct sockaddr_in sin = {
        .sin_family = AF_INET,
        .sin_port   = 0,
        .sin_addr = *((struct in_addr *)host->h_addr_list[0])
    };

    int status = bind(host_Env.sockfd
            , (struct sockaddr *)&sin,
            sizeof(struct sockaddr));
    if (status == -1) {
        ERR_LOG("Socket Bind address failed\n");
        return -1;
    }

    listen(host_Env.sockfd
            , host_Env.nNodes);

    struct sockaddr_in serverinfo = {0};
    size_t len = sizeof(serverinfo);
    getsockname(host_Env.sockfd,(struct sockaddr *)&serverinfo,
                (socklen_t *)&len);
    host_Env.my_port = ntohs(serverinfo.sin_port);

    return 0;
}


void clean_exit(int exitcode)
{
    
    if (host_Env.sockpoll != NULL) close(host_Env.sockfd);

    if (exitcode) {
        ERR_LOG("Exit code : %d, force cleaning\n", exitcode);

        if (exitcode != 0){
            for(int i = 0; host_Env.childproc != NULL && i < host_Env.nNodes; i++){
                kill(host_Env.childproc[i], SIGKILL);
            }
        }
    }

    exit(exitcode);
}


int main(int argc, char *argv[])
{
    bzero(&host_Env,sizeof(_Env));
    host_Env.hostfile = DEFAULT_FILE;
    host_Env.nNodes = 1;

    parse_args(argc, argv, &(host_Env.nNodes), &(host_Env.hostfile),
               &(host_Env.logfile), &(host_Env.exefile),
               &(host_Env.nodeargc), &(host_Env.nodeargv));

    if (host_Env.logfile != NULL )
    {
        int logfd = open(host_Env.logfile, O_RDWR | O_CREAT,S_IRWXU);
        dup2(logfd,fileno(stderr));
        close(logfd);
    }

    host_Env.sockpoll = (struct pollfd *) malloc(sizeof(struct pollfd) *
                                                         host_Env.nNodes);
    bzero(host_Env.sockpoll ,sizeof(struct pollfd) * host_Env.nNodes);
    host_Env.sync_tag_arr = (sync_t *)malloc(sizeof(sync_t) *
                                                     (host_Env.nNodes ));
    bzero(host_Env.sync_tag_arr,sizeof(sync_t) * (host_Env.nNodes ));
    host_Env.nbarrier = host_Env.nNodes;
    host_Env.childproc = malloc(host_Env.nNodes * sizeof(pid_t));

    for(int i = 0;i < host_Env.nNodes; i ++)
        host_Env.sockpoll[i].fd = -1;

    init_signal_handle();

    init_socket();

    get_remote_hosts(host_Env.hostfile, &(host_Env.nhost),
                     &(host_Env.hostlist));

    start_nodes(host_Env.hostlist, host_Env.nhost, host_Env.nNodes,
                host_Env.exefile, host_Env.nodeargc, host_Env.nodeargv,
                host_Env.my_hostname, host_Env.my_port);

    init_sharedmemory();

    dsm_alloc_run();
    return 0;
}
