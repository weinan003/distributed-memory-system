#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <time.h>

#include "sm.h"
#include "sm_common.h"



#define DEBUG_PRINTF(fmt, ...) do{\
	fprintf(stderr,"Client %d: ",cli_env.id);\
	fprintf(stderr,(fmt), ##__VA_ARGS__);\
	fflush(stderr);\
}while(0)

#ifdef DEBUG
#define DEBUG_LOG(fmt, ...) DEBUG_PRINTF(fmt, ##__VA_ARGS__)
#else
#define DEBUG_LOG(fmt,...)
#endif

#define ERR_LOG(fmt, ...) DEBUG_PRINTF(fmt, ##__VA_ARGS__)


#define GET_PAGE_ID(addr) ((addr - cli_env.shared_mem) / (cli_env.pagesize))
#define PAGEID_TO_ADDR(pageid) ((cli_env.shared_mem) + (cli_env.pagesize) * (pageid))

/* Page mark definition and related macros */
typedef unsigned char pagemark_t;
#define PAGEMARK_BIT_SIZE sizeof(pagemark_t) * 8
#define IS_READABLE(pageid) cli_env.pagemark[(pageid) / PAGEMARK_BIT_SIZE] & (1 << (pageid) % PAGEMARK_BIT_SIZE)
#define SET_READABLE(pageid) cli_env.pagemark[(pageid) / PAGEMARK_BIT_SIZE] |= (1 << (pageid) % PAGEMARK_BIT_SIZE)
#define UNSET_READABLE(pageid) cli_env.pagemark[(pageid) / PAGEMARK_BIT_SIZE] &= (~(1 << (pageid) % PAGEMARK_BIT_SIZE))


#define MAX_BACKOFF 100000 // in nanosec
#define TIMEOUT 30 // in sec
#define MAX_TIMEOUT 3
#define MAX_CONN_RETRY 3


typedef void (*msg_handler_func)(sm_rpc_msg_t*,void*);

typedef struct cli_Env
{
	char* allocator_addr;
	int allocator_port;
	int allocator_sockfd;
	struct sockaddr_in allocator_sockaddr;
	int allocator_socklen;
	int id;
	int peer_number;
	void* shared_mem;

	/* track of page rw authority and the used offset */
	pagemark_t* pagemark; 

	/* store the last page malloc success form allocator server */
	int curpage;
	size_t page_offset;
	int pagesize;

	volatile sm_rpc_type_t waiting_type;
	volatile int spin_lock;

	void *addr_reply;
	int page_id_reply;
	time_t last_msg_time;
	int waiting_page;

	msg_handler_func msg_handlers[SM_RPC_TOTAL];
} cli_Env;
static cli_Env cli_env = {0};
static char tx_buffer[5000];


/*
 * Utility
 */

static void
clean_exit(int exitcode)
{
	if (exitcode != 0 && cli_env.allocator_sockfd != -1) {
		INIT_MSG(msg, SM_RPC_ERROR, cli_env.id, 0, 0, 0);
		sm_send(cli_env.allocator_sockfd, &msg, NULL);
		close(cli_env.allocator_sockfd);
	}

	DEBUG_LOG("Shuttig down with exitcode %d\n", exitcode);
	exit(exitcode);
}


static int
wait_for_alloc(sm_rpc_type_t waittype, int timeout_sec, int retry)
{
	sigset_t waitMask;
	sigfillset(&waitMask);
	sigdelset(&waitMask,SIGIO);
	cli_env.spin_lock ++;

	cli_env.waiting_type = waittype;

	struct timespec timeout = {
		.tv_sec = TIMEOUT,
		.tv_nsec = 0,
	};
	siginfo_t info;

	while(cli_env.spin_lock > 0) {
		sigtimedwait(&waitMask, &info, &timeout);
		if (cli_env.spin_lock > 0){
			time_t time_to_last_mst = time(NULL) - cli_env.last_msg_time;
			if (time_to_last_mst <= TIMEOUT) continue;
			else if (time_to_last_mst >TIMEOUT && 
						time_to_last_mst < TIMEOUT + timeout_sec * retry)
			{
				INIT_MSG(msg, SM_RPC_HEART, cli_env.id, 0,0,0);
				sm_send(cli_env.allocator_sockfd, &msg, 0);
                DEBUG_LOG("waiting_type = %d\n",cli_env.waiting_type);
			}
			else{
				cli_env.spin_lock--;
				return 1;
			}
		}
	}
	return 0;
}


static void
resume_from_waiting()
{
	cli_env.waiting_type = SM_RPC_NULL;
	cli_env.spin_lock --;
}


static void
page_read_grant(sm_rpc_msg_t *msg, void *buffer)
{
	int pageid = msg->para1;
	if (pageid == -1){
		ERR_LOG("request for page read failed, exit\n");
		clean_exit(1);
	}
	DEBUG_LOG("receiving read permission for %d\n",pageid);
	void *addr = PAGEID_TO_ADDR(pageid); 
	SET_READABLE(pageid);
	mprotect(addr, cli_env.pagesize, PROT_WRITE | PROT_READ);

	memcpy(addr, buffer,  cli_env.pagesize);

	mprotect(addr, cli_env.pagesize, PROT_READ);
}

static void
page_write_grant(sm_rpc_msg_t *msg, void *buffer)
{
	int pageid = msg->para1;
	int failedpageid = (int)msg->para2;
	if (pageid >= 0 && pageid < PAGENUMS){
		DEBUG_LOG("receiving ownership of %d\n",pageid);
		mprotect(PAGEID_TO_ADDR(pageid), cli_env.pagesize, 
						PROT_READ | PROT_WRITE);
	}
	else {
		DEBUG_LOG("requesting ownership failed, restart from read\n");
//        UNSET_READABLE(failedpageid);
		/* Randomly sleep up to 1ms to avoid competition */
		struct timespec t = {
			.tv_sec = 0,
			.tv_nsec = rand() % MAX_BACKOFF
		};
		nanosleep(&t, NULL);	
		
	}
}


static void
revoke_read(sm_rpc_msg_t *msg, void *buffer)
{
	int pageid = msg->para1;
    if(IS_READABLE(pageid))
    {
        DEBUG_LOG("invalidate %d\n",pageid);
        UNSET_READABLE(pageid);
        mprotect(PAGEID_TO_ADDR(pageid), cli_env.pagesize, PROT_NONE);
    }
}


static void
revoke_write(sm_rpc_msg_t *msg, void *buffer)
{
	int pageid = msg->para1;
	void *addr = (void *)PAGEID_TO_ADDR(pageid);
	DEBUG_LOG("releasing ownership of %d\n",pageid);
	INIT_MSG(rmsg, SM_RPC_PAGE_RETURN, cli_env.id, cli_env.pagesize, pageid, 
					NULL);
	sm_send(cli_env.allocator_sockfd, &rmsg, addr);

	mprotect(PAGEID_TO_ADDR(pageid), cli_env.pagesize, PROT_READ);
}


static void
init_reply_handler(sm_rpc_msg_t *msg, void *buffer)
{
	cli_env.id = msg->para1;
	cli_env.shared_mem = msg->para2;
}


static void
bcast_handler(sm_rpc_msg_t *msg, void *buffer)
{
	cli_env.addr_reply = msg->para2;
}


static void
malloc_handler(sm_rpc_msg_t *msg, void *buffer)
{
	cli_env.page_id_reply = msg->para1;

}


static void
kill_handler(sm_rpc_msg_t *msg, void *buffer)
{
	clean_exit(1);
}


static void
empty_handler(sm_rpc_msg_t *msg, void *buffer)
{
	return;
}


/*
 * Signal Handlers
 */

static void
alloc_msg_handler()
{
	sm_rpc_msg_t msg;
	ssize_t sz = sm_recv(cli_env.allocator_sockfd, &msg, tx_buffer);

	if(sz == 0)
	{
		ERR_LOG("Server stop,client self killed,waiting type = %d,spin = %d\n",cli_env.waiting_type,cli_env.spin_lock);
		clean_exit(1);
	}
	else if (sz < 0) return;

	do {
		cli_env.last_msg_time = time(NULL);
		
		if (cli_env.msg_handlers[msg.type] == NULL){
			ERR_LOG("UNKNOWN MESSAGE TYPE %d, EXIT!\n", msg.type);
			clean_exit(1);
		}
		else
		{
			cli_env.msg_handlers[msg.type](&msg, tx_buffer);
			if (cli_env.waiting_type == msg.type) resume_from_waiting();
		}

        bzero(&msg,sizeof(msg));
        sz = sm_recv(cli_env.allocator_sockfd, &msg, tx_buffer);
	}while(sz > 0);
}


void
segfault_handler(siginfo_t *si)
{
	int pageid = GET_PAGE_ID(si->si_addr);
	int err;
	sigset_t oldsig;
	sigset_t sigmask;
	
	sigemptyset(&sigmask);
	sigaddset(&sigmask, SIGIO);
	
	sigprocmask(SIG_BLOCK, &sigmask, &oldsig);


	int retry = 0;
	if(pageid >= 0 && pageid < PAGENUMS)
	{
		if(IS_READABLE(pageid))
		{
			DEBUG_LOG("write fault @ %d\n",pageid);
			INIT_MSG(msg,SM_SEG_WFAULT,cli_env.id,0,pageid,0);
			sigprocmask(SIG_SETMASK, &oldsig, NULL);
				while(retry < 3){
					sm_send(cli_env.allocator_sockfd,&msg,NULL);
					err = wait_for_alloc(SM_SEG_WFAULT_GRANT, 0, 0);
					if (err == 0) break;
					retry++;
				}
			sigprocmask(SIG_BLOCK, &sigmask, &oldsig);
		}
		else
		{
			DEBUG_LOG("read fault @ %d\n",pageid);
			INIT_MSG(msg,SM_SEG_RFAULT,cli_env.id,0,pageid,0);
			sigprocmask(SIG_SETMASK, &oldsig, NULL);
				while(retry < 3){
					sm_send(cli_env.allocator_sockfd,&msg,NULL);
					err = wait_for_alloc(SM_SEG_RFAULT_GRANT, 0, 0);
					if (err == 0) break;
					retry++;
				}
			sigprocmask(SIG_BLOCK, &sigmask, &oldsig);
		}

		if (err) {
			DEBUG_LOG("Sending request failed, exit\n");
			sigprocmask(SIG_SETMASK, &oldsig, NULL);
			clean_exit(1);
		}
	}
	else
	{
		ERR_LOG("Segmentation Fault: %p\n",si->si_addr);
		sigprocmask(SIG_SETMASK, &oldsig, NULL);
		clean_exit(1);
	}
	sigprocmask(SIG_SETMASK, &oldsig, NULL);
}

void
sig_handler(int signum, siginfo_t *si, void *ctx)
{
	switch(signum)
	{
		case SIGIO:
			if (si->si_code == POLL_HUP){
				clean_exit(0);
			}
			alloc_msg_handler();
			break;
		case SIGSEGV:
			segfault_handler(si);
			break;
		default:
			break;
	}
}



/*
 * Connection functions
 */

static int
connect_to_allocator()
{
	struct hostent *host;

	host = gethostbyname(cli_env.allocator_addr);

	cli_env.allocator_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in tmp_sockaddr = {
		.sin_family = AF_INET,
		.sin_port   = htons(cli_env.allocator_port),
		.sin_addr   = *((struct in_addr *)host->h_addr_list[0])
	};

	bzero(&(tmp_sockaddr.sin_zero), 8);
	if(connect(cli_env.allocator_sockfd, (struct sockaddr *)&tmp_sockaddr,
			   sizeof(struct sockaddr)))
	{
		close(cli_env.allocator_sockfd);
		return -1;
	}

	cli_env.last_msg_time = time(NULL);
	memcpy(&(cli_env.allocator_sockaddr),&tmp_sockaddr,
			sizeof(struct sockaddr_in));

	/* enable SIGPOLL on the socket */

	fcntl(cli_env.allocator_sockfd, F_SETFL, O_ASYNC);
	fcntl(cli_env.allocator_sockfd, F_SETOWN, getpid());

    return 0;
}


static void
register_signal_handlers()
{
    struct sigaction sig_config = {
            .sa_flags = SA_SIGINFO,
            .sa_handler = sig_handler
    };
    sigemptyset(&sig_config.sa_mask);


    if(sigaction (SIGIO, &sig_config, NULL) == -1) {
        ERR_LOG("regist SIGIO failed\n");
    }

    if(sigaction(SIGSEGV, &sig_config, NULL) == -1){
        ERR_LOG("regist SIGSEGV failed\n");
    }

	cli_env.msg_handlers[SM_RPC_INIT_REPLY] = init_reply_handler;
	cli_env.msg_handlers[SM_RPC_EXIT_GRANT] = empty_handler;
	cli_env.msg_handlers[SM_RPC_RESUME] = empty_handler;
	cli_env.msg_handlers[SM_RPC_KILL] = kill_handler;
	cli_env.msg_handlers[SM_RPC_HEART] = empty_handler;
	cli_env.msg_handlers[SM_SEG_RFAULT_GRANT] = page_read_grant;
	cli_env.msg_handlers[SM_SEG_WFAULT_GRANT] = page_write_grant;
	cli_env.msg_handlers[SM_RPC_MALLOC_GRANT] = malloc_handler;
	cli_env.msg_handlers[SM_RPC_BCAST_REPLY] = bcast_handler;
	cli_env.msg_handlers[SM_SEG_REVOK_W] = revoke_write;
	cli_env.msg_handlers[SM_SEG_REVOK_R] = revoke_read;
}


/*
 * Public functions
 */


int sm_node_init(int *argc, char **argv[], int *nodes, int *nid)
{
	static int once_protect = 0;
	if(once_protect)
	{
		ERR_LOG("node %d already invoke sm_node_init function once yet,"
							 "illegal operating,kill process self\n"
					 ,cli_env.id);
		clean_exit(1);
	}
	once_protect++;

	/* unmarshalling server args and delete them from client process's view */
	cli_env.allocator_addr = (*argv)[1];
	cli_env.allocator_port = atoi((*argv)[2]);
	*nodes = cli_env.peer_number = atoi((*argv)[3]);
	cli_env.spin_lock = 0;
    cli_env.pagemark = malloc(sizeof(pagemark_t) * (PAGENUMS + 1)  * 8 + 1);
    bzero(cli_env.pagemark, sizeof(pagemark_t) * (PAGENUMS + 1) * 8+ 1);
	cli_env.pagesize = getpagesize();

	/* register signal handler */
	register_signal_handlers();
	
	/* if connect failed,sleep 1s reconnect */
	int conn_retry = 0;
	while(connect_to_allocator()){
		if(conn_retry > MAX_CONN_RETRY){
			ERR_LOG("connection failed\n");
			clean_exit(1);
		}
		conn_retry++;
		sleep(1);
	}

	void *protect = sbrk(4096);
	mprotect(protect, 4096, PROT_NONE);


	void* heaptop = sbrk(0);
	DEBUG_LOG("HEAP TOP = %p\n",heaptop);
	INIT_MSG(msg, SM_RPC_INIT, -2, 0, 0,heaptop);

	sm_send(cli_env.allocator_sockfd, &msg, NULL);
	int retry = 0;
	int err = 1;
	while(retry < 10 && err != 0){
		err = wait_for_alloc(SM_RPC_INIT_REPLY, 5, 10);
		retry++;
	}
	if (err) {
		ERR_LOG("Sending INIT failed, exit\n");
		clean_exit(1);
	}

	/* message returned by allocator */
    *nid = cli_env.id;


	ERR_LOG("mmapping @ %p, %d pages\n", cli_env.shared_mem, PAGENUMS);
	void *ret = mmap(cli_env.shared_mem, cli_env.pagesize * PAGENUMS + 4096,PROT_NONE,
			MAP_FIXED | MAP_ANONYMOUS | MAP_PRIVATE,-1,0);
	DEBUG_LOG("mmap done\n");
	if(ret == MAP_FAILED || ret != cli_env.shared_mem)
	{
		perror("client map failed");
		clean_exit(1);
	}


	/* hide lib used parameters */
	for(int i = 6;i < *argc;i++)
		(*argv)[i - 5] = (*argv)[i];

	*argc -= 5;

	return 0;
}

void sm_node_exit(void)
{
	INIT_MSG(msg,SM_RPC_EXIT_APPLY,cli_env.id,0,0,0);
	sm_send(cli_env.allocator_sockfd,&msg,NULL);
	if (wait_for_alloc(SM_RPC_EXIT_GRANT, 2, 3)) {
		ERR_LOG("Sending exit failed, exit anyway\n");
		clean_exit(1);
	}
}

void sm_barrier(void)
{
	INIT_MSG(msg,SM_RPC_BARRIER,cli_env.id,0,0,0);
	sm_send(cli_env.allocator_sockfd,&msg,NULL);
	if (wait_for_alloc(SM_RPC_RESUME, 2, 3)) {
		ERR_LOG("Sending barrier failed\n");
		clean_exit(1);
	}
}

static void *
mallocfromlocal(int size)
{
	void *ret = NULL;
	
	/* first invoke after sm_init() */
	if(cli_env.curpage == cli_env.page_offset && cli_env.curpage == 0) return ret;

	if(cli_env.page_offset + size < cli_env.pagesize)
	{
		ret = (void* )(cli_env.shared_mem + 
				cli_env.curpage*cli_env.pagesize + 
				cli_env.page_offset);
		cli_env.page_offset += size;
	}

	return ret;
}

static void *
mallocfromallocator(int size)
{
	int req_num = (size - 1) /cli_env.pagesize + 1;
	INIT_MSG(msg,SM_RPC_MALLOC,cli_env.id,0,req_num,0);
	sm_send(cli_env.allocator_sockfd,&msg,NULL);
	if (wait_for_alloc(SM_RPC_MALLOC_GRANT, 2, 3)) {
		ERR_LOG("Sending malloc request failed, exit\n");
		clean_exit(1);
	}
	
	if(cli_env.page_id_reply == -1)
		return NULL;
	else
	{
		int startpage = cli_env.page_id_reply;
		DEBUG_LOG(" allocated %d-%d\n",startpage, startpage + req_num - 1);
		for(int i = 0;i < req_num;i ++)
		{
			SET_READABLE(startpage + i);
			mprotect(PAGEID_TO_ADDR(startpage + i), cli_env.pagesize,
							PROT_READ| PROT_WRITE);
		}

		cli_env.curpage = (startpage + req_num - 1);
		cli_env.page_offset = (size - 1) % cli_env.pagesize + 1;
		return PAGEID_TO_ADDR(startpage);
	}
}

void *sm_malloc(size_t size)
{
	void* ret;

	ret = mallocfromlocal(size);
	if(!ret)
		ret = mallocfromallocator(size);
	return ret;
}


void sm_bcast(void **addr, int root_nid)
{
	INIT_MSG(msg, SM_RPC_BCAST, cli_env.id,0,0,
					cli_env.id == root_nid ? *addr: 0);
	sm_send(cli_env.allocator_sockfd,&msg,NULL);
	if (wait_for_alloc(SM_RPC_BCAST_REPLY, 2, 3)) {
		ERR_LOG("Sending bcast failed, exit anyway\n");
		clean_exit(1);
	}
	
	*addr = cli_env.addr_reply;
}


