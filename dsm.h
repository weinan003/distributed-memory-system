#ifndef __DSM_ENV_H__
#define __DSM_ENV_H__


#include <unistd.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netdb.h>
#include <poll.h>

#include "sm_common.h"


#define DEBUG_PRINTF(fmt, ...) do{\
    fprintf(stderr,"ALLOCATOR :");\
    fprintf(stderr,(fmt), ##__VA_ARGS__);\
    fflush(stderr);\
}while(0)


#ifdef DEBUG
#define DEBUG_LOG(fmt, ...) DEBUG_PRINTF(fmt, ##__VA_ARGS__)
#else
#define DEBUG_LOG(fmt,...)
#endif // DEBUG

#define ERR_LOG(fmt, ...) DEBUG_PRINTF(fmt, ##__VA_ARGS__)

typedef enum {
    IDLE = 0,
    STOP,
} sync_t;


typedef struct _glb_Env
{
    /* nNode : the number of remote process */
    int nNodes;

    /* nhost : the number of remote real machines */
    int nhost;          

    /* nodeargc : the number of the parameters for remote processes start up */
    int nodeargc;       

    /* recorde the number of node already apply exit */
    int nZNodes;        

    /* hostfile : path of hostfile*/
    char *hostfile;     

    /* logfile : path of log file (can be NULL) */
    char *logfile;      

    /* exefile : path of remote process file */
    char *exefile;      

    /* nodeargv: the parameters for remote processes start up*/
    char **nodeargv;    

    /* remote machine address(ssh address) */
    char **hostlist;    

    /* server ip presented by string */
    char *my_hostname;  

    /* server listen port */
    int my_port;        

    /* socket fd set for poll() */
    struct pollfd *sockpoll;

    /* build connectiong socket */
    int sockfd; 

    /* mmap vir address */
    void* shared_mem;

    /* pagetable only store the write authority socket slot in it*/
    int* pagetable;

    /* store next unused page index ,init value should be 0 */
    int next_alloc;

    /* store stop state for each nodes*/
    sync_t *sync_tag_arr;

    /* semaphore like barrier synchronize tag */
    int nbarrier;

    /* log file fd */
    int logfd;

    /* child processes */
    pid_t *childproc;

    /* msg loop switch tag */
    int finish;
} _Env;

_Env host_Env;

#define LOG(fmt, ...) fprintf(stderr, (fmt), ##__VA_ARGS__)
#define LOG_NODE(nid, fmt, ...) do{ \
    LOG("#%d: ", nid);\
    LOG(fmt, ##__VA_ARGS__);\
}while(0);


#define LOG_INIT LOG("-= %d node processes =-\n", host_Env.nNodes)
#define LOG_MALLOC(nid, ps, pe) LOG_NODE(nid, "allocated %d-%d\n", ps, pe)
#define LOG_RF(nid, p) LOG_NODE(nid, "read fault @ %d\n", p)
#define LOG_WF(nid, p) LOG_NODE(nid, "write fault @ %d\n", p)
#define LOG_RELEASE(nid, p) LOG_NODE(nid, "releasing ownership of %d\n", p)
#define LOG_RECV_RD(nid, p) LOG_NODE(nid, "receiving read permission for %d\n", p)
#define LOG_RECV_OWN(nid, p) LOG_NODE(nid, "receiving ownership of %d", p)


void clean_exit(int exitcode);
void dsm_alloc_run();

void init_signal_handle(void );
void sig_chld(int );
void sig_pipe(int );
void sig_int(int );


void    alloc_msg_handler(int i,sm_rpc_msg_t*);
void    alloc_handle_bcast(int ,sm_rpc_msg_t* );
void    alloc_handle_rfault(int ,sm_rpc_msg_t*);
void    alloc_handle_wfault(int ,sm_rpc_msg_t*);
int     alloc_handle_barrier(int ,sm_rpc_msg_t*);
int     alloc_handle_error(int );
int     alloc_handle_exit(int );
int     alloc_handle_pageback(int ,sm_rpc_msg_t* );
int     alloc_handle_malloc(int ,sm_rpc_msg_t* );
#endif
