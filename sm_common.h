
#ifndef __SM_COMMON_H
#define __SM_COMMON_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/uio.h>

#define INIT_MSG(MSG,T,N,P,P1,P2) sm_rpc_msg_t MSG = {\
    .type = (sm_rpc_type_t)T,\
    .nid = (int)N,\
    .payloadlen = (size_t)P,\
    .para1 = P1,\
    .para2 = P2\
}

#define PAGENUMS 0xffff 

typedef enum {
    SM_RPC_NULL = 0,
    SM_RPC_INIT = 1,
    SM_RPC_BARRIER = 2,
    SM_RPC_EXIT_APPLY = 3,
    SM_RPC_EXIT_GRANT = 4,
    SM_RPC_RESUME = 5,
    SM_RPC_BCAST = 6,
    SM_RPC_KILL = 7,
    SM_RPC_HEART = 8,
    SM_SEG_RFAULT = 9,
    SM_SEG_RFAULT_GRANT = 10,
    SM_SEG_WFAULT = 11,
    SM_SEG_WFAULT_GRANT = 12,
    SM_RPC_MALLOC = 13,
    SM_RPC_MALLOC_GRANT = 14,
	SM_RPC_BCAST_REPLY = 15,
    SM_SEG_REVOK_W = 16,
    SM_SEG_REVOK_R = 17,
	SM_RPC_PAGE_RETURN = 18,
	SM_RPC_INIT_REPLY = 19,
	SM_RPC_ERROR = 20,

	/* DUMMY TYPE */
	SM_RPC_TOTAL
}sm_rpc_type_t;


typedef struct sm_rpc_message {
    sm_rpc_type_t type;
    int nid;
    size_t payloadlen;
    int para1;
    void* para2;
}sm_rpc_msg_t;
#define MSGLEN sizeof(sm_rpc_type_t)+sizeof(int)+sizeof(size_t)+sizeof(void*)+\
				sizeof(int)

static char buffer[100] = {0};


/* Serialization of protocol message.
 * @param msg struct to contain all information of the message
 * @param buf the serialized message
 * Caller need to be sure buffer has enough space to contain the whole msg
 * */
static inline size_t
sm_rpc_msg_seri(sm_rpc_msg_t *msg)
{
    size_t ptr = 0;

    memcpy(buffer + ptr, &(msg->type), sizeof(msg->type));
    ptr += sizeof(msg->type);
    memcpy(buffer + ptr, &(msg->nid), sizeof(msg->nid));
    ptr += sizeof(msg->nid);
    memcpy(buffer + ptr, &(msg->payloadlen), sizeof(msg->payloadlen));
    ptr += sizeof(msg->payloadlen);
    memcpy(buffer + ptr, &(msg->para1), sizeof(msg->para1));
    ptr += sizeof(msg->para1);
    memcpy(buffer + ptr, &(msg->para2), sizeof(msg->para2));
    ptr += sizeof(msg->para2);

    return ptr;
}

/* Deserialize message
 * @param msg Struct of message to be put into
 * @param buf Received serialized message
 * msg->payload need to be initialized and has enough space
 * */
static inline size_t
sm_rpc_msg_deseri(sm_rpc_msg_t *msg, char *buf)
{
    size_t ptr = 0;
    memcpy(&(msg->type), &(buf[ptr]), sizeof(msg->type));
    ptr += sizeof(msg->type);
    memcpy(&(msg->nid), &(buf[ptr]), sizeof(msg->nid));
    ptr += sizeof(msg->nid);
    memcpy(&(msg->payloadlen), &(buf[ptr]), sizeof(msg->payloadlen));
    ptr += sizeof(msg->payloadlen);
    memcpy(&(msg->para1),&(buf[ptr]), sizeof(msg->para1));
    ptr += sizeof(msg->para1);
    memcpy(&(msg->para2),&(buf[ptr]), sizeof(msg->para2));
    ptr += sizeof(msg->para2);

    return ptr;
};


static inline ssize_t
sm_send(int sock, sm_rpc_msg_t *msg, void *payload)
{
    size_t msglen = sm_rpc_msg_seri(msg);
    ssize_t sentsz = 0;
	
	while(sentsz < msglen)
	{
		ssize_t tmplen = send(sock, msg + sentsz, msglen-sentsz,0);
		if (tmplen > 0) sentsz += tmplen;
	}

	ssize_t payloadsent = 0;
	while(payloadsent < msg->payloadlen)
	{
		ssize_t tmplen = send(sock, payload + payloadsent, msg->payloadlen - payloadsent, 0);
		if (tmplen > 0) payloadsent += tmplen;
	}

	sentsz += payloadsent;
    if (sentsz != msglen + msg->payloadlen)
    {
        return -1;
    }

    return sentsz;
}


static inline ssize_t
sm_recv(int sock, sm_rpc_msg_t *msg, void *payload)
{
	size_t tmpsz;
    ssize_t recvsz = recv(sock, buffer, MSGLEN, MSG_DONTWAIT);
	if (recvsz <= 0) return recvsz;
	while(recvsz < MSGLEN){
		tmpsz = recv(sock, buffer+recvsz, MSGLEN-recvsz, 0);
		if (tmpsz > 0) recvsz += tmpsz;
	}

    sm_rpc_msg_deseri(msg, buffer);
	ssize_t payloadsz = 0;
    while (payload && payloadsz < msg->payloadlen) {
        tmpsz = recv(sock, payload + payloadsz, 
						msg->payloadlen - payloadsz, 0);
		if (tmpsz > 0) payloadsz += tmpsz;
    }
	
	recvsz += payloadsz;

    if (recvsz != MSGLEN + msg->payloadlen){
		fprintf(stderr,"error in recv!\n");
		exit(1);
    }

    return recvsz;
}
#endif //__SM_COMMON_H
